import os
import getpass
import tzlocal
import random

def generate_port():
    return random.randint(1024, 49151)

def print_Userinfo(Userinfo_temp,Userinfo_plot):
    print("Copy your temp directory:\n")
    for i in range(0,len(Userinfo_temp)):
        print(Userinfo_temp[i]+"\n")
    print("Copy your plots directory:\n")
    for i in range(0, len(Userinfo_plot)):
        print(Userinfo_plot[i] + "\n")
    input("Enter any key to continue...")

def createDirs(temp_arr, end_arr):
    #-v {MY_PLOTTING_DRIVE}:/plotting:rw -v {MY_PLOTS_DRIVE}:/plots:rw
    Userinfo_temp = []
    Userinfo_plot = []
    temp_str = ""
    end_str = ""
    for i in range(0, len(temp_arr)):
        temp_str += f'-v {temp_arr[i]}:/plotting{i}:rw '
        Userinfo_temp.append(f'/plotting{i}')
    for i in range(0, len(end_arr)):
        end_str += f'-v {end_arr[i]}:/plots{i}:rw '
        Userinfo_plot.append(f'/plots{i}')

    print_Userinfo(Userinfo_temp,Userinfo_plot)

    return temp_str + end_str


USER = getpass.getuser()

TIME_ZONE_NAME = tzlocal.get_localzone().zone

PORT = generate_port()

arr_pathTemp =[]
arr_pathPlots = []

plotTemp = input("Enter the number of temporary directories: ")
plotEnd = input("Enter the number of end plots directories: ")

for i in range(0, int(plotTemp)):
    MY_PLOTTING_DRIVE = input("Enter your temp ssd or hdd plotting directory: ")
    arr_pathTemp.append(MY_PLOTTING_DRIVE)

for i in range(0, int(plotEnd)):
    MY_PLOTS_DRIVE = input("Enter your final hdd plot directory: ")
    arr_pathPlots.append(MY_PLOTS_DRIVE)

result_dir_str = createDirs(arr_pathTemp,arr_pathPlots)

os.system(f'docker run --name ecopoolplotter{PORT} -t -p {PORT}:8926 -e TZ={TIME_ZONE_NAME} -v C:\\Users\\{USER}\\.ecopoolplotter:/root/.chia:rw {result_dir_str}-e mode=plotter -d plotter')

