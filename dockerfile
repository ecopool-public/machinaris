# set ubuntu release version
ARG UBUNTU_VER="20.04"

######## packages stage ###########
FROM ubuntu:${UBUNTU_VER} as package_stage

ARG DEBIAN_FRONTEND=noninteractive

# install build packages
RUN \
	apt-get update \
	&& apt-get install -y \
		acl \
		ansible \
		apt \
		bash \
		bc \
		build-essential \
		ca-certificates \
		curl \
		git \
		jq \
		nfs-common \
		openssl \
		python3 \
		python3.8-distutils \
		python3.8-venv \
		python3-dev \
		python3-pip \
		python-is-python3 \
		sqlite3 \
		sudo \
		tar \
		tzdata \
		unzip \
		vim \
		wget \
	\
# cleanup apt cache
	\
	&& rm -rf \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

######## build/runtime stage #########
FROM package_stage
# Base install of official Chia binaries at the given branch
ARG CHIA_BRANCH

# copy local files
COPY . /machinaris/

# set workdir
WORKDIR /chia-blockchain

# install chia
RUN \
	git clone https://github.com/Chia-Network/chia-blockchain.git /chia-blockchain \
	&& git submodule update --init mozilla-ca \
	&& chmod +x install.sh \
	&& /usr/bin/sh ./install.sh \
	\
# cleanup apt and pip caches
	\
	&& rm -rf \
		/root/.cache \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

#WORKDIR /chia-blockchain/venv/lib/python3.8/site-packages
#RUN	\
#    curl -O https://gitlab.com/ecopool-public/user-info/-/raw/master/plotter/chiapos.cpython-38-x86_64-linux-gnu.so



WORKDIR /chia-blockchain
# install additional tools such as Plotman, Chiadog, and Machinaris
RUN \
	. /machinaris/scripts/chiadog_install.sh \
	&& . /machinaris/scripts/plotman_install.sh \
	&& . /machinaris/scripts/machinaris_install.sh \
	\
# cleanup apt and pip caches
	\
	&& rm -rf \
		/root/.cache \
		/tmp/* \
		/var/lib/apt/lists/* \
		/var/tmp/*

# Provide a colon-separated list of in-container paths to your mnemonic keys
ENV keys="/root/.chia/mnemonic.txt"  
# Provide a colon-separated list of in-container paths to your completed plots
ENV plots_dir="/plots"
# One of fullnode, plotter, farmer, or harvester. Default is fullnode
ENV mode="fullnode" 
# If mode=plotter, optional 2 public keys will be set in your plotman.yaml
ENV farmer_pk="97d571995423d6c5620afcf3b8a9e29f95dcb4f5a94ee6df67cc6edebec2e17851937ed62c054783e0f1bd007400851c"
ENV pool_pk="a8f696e15a43cd45c5bf142b75de328a1931d926bd17ee11ca8c9db7d7f34cecacd1010db0c69c2f39cdde126cc05d2c"
# If mode=harvester, required for host and port the harvester will your farmer
ENV farmer_address="null"
ENV farmer_port="null"
# Only set true if using Chia's old test for testing only, default uses mainnet
ENV testnet="false"

ENV PATH="${PATH}:/chia-blockchain/venv/bin"
ENV TZ=Etc/UTC
ENV FLASK_ENV=production
ENV FLASK_APP=/machinaris/main.py
ENV XDG_CONFIG_HOME=/root/.chia

# ports
EXPOSE 8555
EXPOSE 8444
EXPOSE 8926

WORKDIR /chia-blockchain
ENTRYPOINT ["bash", "./entrypoint.sh"]
